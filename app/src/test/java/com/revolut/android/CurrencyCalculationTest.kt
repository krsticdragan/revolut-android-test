package com.revolut.android

import com.revolut.android.model.Currency
import com.revolut.android.ui.fragment.converter.ConverterPresenter
import org.junit.Assert
import org.junit.Test

class CurrencyCalculationTest {

    private val presenter: ConverterPresenter = ConverterPresenter()
    private val eur = Currency("EUR", 1.2)

    @Test
    fun convert100Euros() {
        val convertZeroValue = presenter.calculateRate(eur.currencyRate, 100.0)
        Assert.assertEquals(convertZeroValue, 120.0, 0.0)
    }

    @Test
    fun convert10Euros() {
        val convertZeroValue = presenter.calculateRate(eur.currencyRate, 10.0)
        Assert.assertEquals(convertZeroValue, 12.0, 0.0)
    }

    @Test
    fun convert0Euros() {
        val convertZeroValue = presenter.calculateRate(eur.currencyRate, 0.0)
        Assert.assertEquals(convertZeroValue, 0.0, 0.0)
    }

    @Test
    fun convert0Rate() {
        val convertStringValue = presenter.calculateRate(0.0, 100.0)
        Assert.assertEquals(convertStringValue, 0.0, 0.0)
    }

}