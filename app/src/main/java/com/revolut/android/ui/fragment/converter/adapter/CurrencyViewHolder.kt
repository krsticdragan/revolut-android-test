package com.revolut.android.ui.fragment.converter.adapter

import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.widget.RxTextView
import com.revolut.android.model.Currency
import com.revolut.android.ui.fragment.converter.ConverterPresenter
import com.revolut.android.utils.CurrencyUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_converter.view.*

class CurrencyViewHolder(
    view: View,
    val presenter: ConverterPresenter
) : RecyclerView.ViewHolder(view) {


    fun bind(currency: Currency) {

        val totalCurrencyValue = presenter.calculateRate(currency.currencyRate, presenter.getCurrencyValue())

        Glide.with(itemView)
            .load(CurrencyUtils.getCurrencyImage(currency.currencyName))
            .into(itemView.ivCurrency)

        itemView.tvCurrencyNameShort.text = currency.currencyName
        itemView.tvCurrencyNameFull.text = CurrencyUtils.getCurrencyFullName(currency.currencyName)

        if (totalCurrencyValue == 0.0) {
            itemView.etCurrencyValue.text!!.clear()
        } else {
            itemView.etCurrencyValue.setText(String.format("%.1f", totalCurrencyValue))
        }

        if (currency.currencyName == presenter.getSelectedCurrency()) {
            CompositeDisposable().add(
                RxTextView.textChanges(itemView.etCurrencyValue)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe {
                        presenter.updateSelectedCurrency(currency.currencyName, it)
                    })
        }

        itemView.etCurrencyValue.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    presenter.selectNewCurrency(Currency(currency.currencyName, 1.0))
                    itemView.etCurrencyValue.text!!.clear()
                }
            }
            false
        }
    }
}