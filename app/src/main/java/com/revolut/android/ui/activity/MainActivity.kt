package com.revolut.android.ui.activity

import android.os.Bundle
import com.revolut.android.R
import com.revolut.android.utils.BaseActivity

class MainActivity : BaseActivity(), MainContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

}
