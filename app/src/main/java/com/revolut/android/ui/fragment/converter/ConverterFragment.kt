package com.revolut.android.ui.fragment.converter

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.revolut.android.R
import com.revolut.android.model.Currency
import com.revolut.android.ui.fragment.converter.adapter.CurrencyAdapter
import com.revolut.android.utils.BaseFragment
import kotlinx.android.synthetic.main.fragment_converter.*
import javax.inject.Inject

class ConverterFragment : BaseFragment(), ConverterContract.View {

    private var adapter: CurrencyAdapter? = null

    @Inject
    lateinit var presenter: ConverterPresenter

    override fun getLayoutResource(): Int {
        return R.layout.fragment_converter
    }

    override fun onResume() {
        presenter.init(compositeDisposable)
        super.onResume()
    }

    override fun setupAdapter(
        rates: MutableList<Currency>,
        active: Boolean
    ) {
        if (adapter != null) {
            adapter!!.rates = rates
            updateAdapter(active)
        } else {
            adapter = CurrencyAdapter(rates, presenter)
            rv_currency.layoutManager = LinearLayoutManager(context)
            rv_currency.setItemViewCacheSize(30)
            (rv_currency.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            rv_currency.adapter = adapter
        }
        setError(false)
    }

    override fun updateAdapter(newCurrency: Boolean) {
        if (!newCurrency) {
            adapter!!.notifyItemRangeChanged(1, adapter!!.rates.size - 1)
        } else {
            adapter!!.notifyDataSetChanged()
            rv_currency.scrollToPosition(0)
        }
    }

    override fun setErrorMessage(error: String) {
        setError(true)
        tv_error.text = error
    }

    override fun setError(isError: Boolean) {
        rv_currency.visibility = if (isError) {
            View.GONE
        } else {
            View.VISIBLE
        }
        tv_error.visibility = if (isError) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun onPause() {
        presenter.deinit()
        super.onPause()
    }

}