package com.revolut.android.ui.fragment.converter.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.revolut.android.R
import com.revolut.android.model.Currency
import com.revolut.android.ui.fragment.converter.ConverterPresenter

class CurrencyAdapter(
    var rates: MutableList<Currency>,
    val presenter: ConverterPresenter
) : RecyclerView.Adapter<CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_converter, parent, false)
        return CurrencyViewHolder(itemView, presenter)
    }

    override fun getItemCount(): Int {
        return rates.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(rates[position])
    }

}