package com.revolut.android.ui.fragment.converter

import com.revolut.android.model.Currency
import io.reactivex.disposables.CompositeDisposable

interface ConverterContract {

    interface View {

        fun setupAdapter(
            rates: MutableList<Currency>,
            active: Boolean
        )

        fun updateAdapter(newCurrency: Boolean)

        fun showToast(text: String?)

        fun setErrorMessage(error: String)

        fun setError(isError: Boolean)

    }

    interface Presenter {

        fun init(compositeDisposable: CompositeDisposable)

        fun deinit()

        fun updateRates(currency: Currency, isNewCurrency: Boolean)

        fun startUpdateRateTimer()

        fun updateAdapter(update: Boolean)

        fun calculateRate(rate:Double, value:Double): Double

        fun selectNewCurrency(currency: Currency)

        fun updateSelectedCurrency(currencyName: String, value: CharSequence)

        fun saveCurrencyValue(value:Double)

        fun getCurrencyValue(): Double

        fun saveSelectedCurrency(selectedCurrency: String)

        fun getSelectedCurrency(): String
    }

}