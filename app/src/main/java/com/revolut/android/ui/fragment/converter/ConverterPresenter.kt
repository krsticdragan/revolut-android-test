package com.revolut.android.ui.fragment.converter

import android.text.TextUtils
import com.revolut.android.R
import com.revolut.android.api.Api
import com.revolut.android.app.App
import com.revolut.android.app.Preferences
import com.revolut.android.model.Currency
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ConverterPresenter @Inject constructor() : ConverterContract.Presenter {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var view: ConverterContract.View

    @Inject
    lateinit var preferences: Preferences

    private lateinit var disposable: CompositeDisposable

    private var currencyList: MutableList<Currency> = mutableListOf()

    override fun init(compositeDisposable: CompositeDisposable) {
        disposable = if (compositeDisposable.isDisposed) {
            CompositeDisposable()
        } else {
            compositeDisposable
        }
        startUpdateRateTimer()
    }

    override fun startUpdateRateTimer() {
        disposable.add(Observable.interval(1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .startWith(0)
            .subscribe {
                updateRates(Currency(getSelectedCurrency(), 1.0), false)
            })
    }

    override fun updateAdapter(update: Boolean) {
        view.updateAdapter(update)
    }

    override fun calculateRate(rate: Double, value: Double): Double {
        if (value.toString().isNotEmpty()) {
            return value * rate
        }
        return 0.0
    }

    override fun selectNewCurrency(currency: Currency) {
        saveSelectedCurrency(currency.currencyName)
        saveCurrencyValue(0.0)
        updateRates(currency, true)
    }

    override fun updateSelectedCurrency(currencyName: String, value: CharSequence) {
        if (TextUtils.isDigitsOnly(value)) {
            if (!TextUtils.isEmpty(value)) {
                saveCurrencyValue(value.toString().toDouble())
                updateRates(Currency(currencyName, 1.0), false)
            } else {
                saveCurrencyValue(0.0)
                updateRates(Currency(currencyName, 1.0), false)
            }
        }
    }

    override fun updateRates(currency: Currency, isNewCurrency: Boolean) {
        disposable.add(
            api.getConvertData(getSelectedCurrency())
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    currencyList.clear()
                    currencyList.add(currency)
                    currencyList.addAll(response.getRateList())
                    view.setupAdapter(currencyList, isNewCurrency)
                }, { error ->
                    view.setErrorMessage(error.localizedMessage)
                })
        )
    }

    override fun saveCurrencyValue(value: Double) {
        preferences.storeLastCurrencyValue(value)
    }

    override fun saveSelectedCurrency(selectedCurrency: String) {
        preferences.storeLastCurrency(selectedCurrency)
    }

    override fun getSelectedCurrency(): String {
        return preferences.getLastCurrency()
    }

    override fun getCurrencyValue(): Double {
        return preferences.getLastCurrencyValue()
    }

    override fun deinit() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
    }

}