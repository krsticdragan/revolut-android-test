package com.revolut.android.app.modules

import android.app.Application
import com.revolut.android.app.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: App): Application {
        return app
    }

}