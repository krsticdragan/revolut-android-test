package com.revolut.android.app.modules.view.activity

import com.revolut.android.ui.activity.MainActivity
import com.revolut.android.ui.activity.MainContract
import com.revolut.android.ui.activity.MainPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class MainActivityModule {

    @Binds
    abstract fun provideMainActivityPresenter(mainPresenter: MainPresenter): MainContract.Presenter

    @Binds
    abstract fun provideMainActivityView(mainActivity: MainActivity): MainContract.View

}