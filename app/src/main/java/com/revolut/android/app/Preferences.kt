package com.revolut.android.app

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.revolut.android.utils.Constants

class Preferences(app: Application) {

    private var preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    fun get() : SharedPreferences {
        return preferences
    }

    fun storeLastCurrency(currency: String) {
        preferences.edit().putString(Constants.PREF_LAST_CURRENCY, currency).apply()
    }

    fun getLastCurrency(): String {
        return preferences.getString(Constants.PREF_LAST_CURRENCY, Constants.DEFAULT_CURRENCY)!!
    }

    fun storeLastCurrencyValue(value: Double) {
        preferences.edit().putFloat(Constants.PREF_LAST_CURRENCY_VALUE, value.toFloat()).apply()
    }

    fun getLastCurrencyValue(): Double {
        return preferences.getFloat(Constants.PREF_LAST_CURRENCY_VALUE, 1f).toDouble()
    }
}