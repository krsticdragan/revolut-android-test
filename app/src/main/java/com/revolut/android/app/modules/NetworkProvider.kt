package com.revolut.android.app.modules

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.revolut.android.BuildConfig
import com.revolut.android.R
import com.revolut.android.api.Api
import com.revolut.android.app.App
import com.revolut.android.utils.ConnectivityInterceptor
import com.revolut.android.utils.Constants
import com.revolut.android.utils.NoConnectivityException
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkProvider {

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        try {
            builder.connectTimeout((120 * 1000).toLong(), TimeUnit.MILLISECONDS)
                .readTimeout((120 * 1000).toLong(), TimeUnit.MILLISECONDS)
                .addInterceptor(getLoggingInterceptor())
                .addInterceptor(ConnectivityInterceptor(App.instance.applicationContext, this))
        } catch (exc: Exception) {
            if (exc is NoConnectivityException) {
                Log.e(
                    App.instance.getString(R.string.error_no_internet_connection),
                    exc.localizedMessage
                )
            }
        }
        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideRestAdapter(): Retrofit {
        val builder = Retrofit.Builder()
        builder.baseUrl(Constants.BASE_URL)
            .client(provideOkHttpClient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }


    @Provides
    @Singleton
    internal fun provideNetworkService(restAdapter: Retrofit): Api {
        return restAdapter.create(Api::class.java)
    }

    private fun getLoggingInterceptor(): Interceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return interceptor
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }
}