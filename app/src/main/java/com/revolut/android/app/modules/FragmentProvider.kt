package com.revolut.android.app.modules

import com.revolut.android.app.modules.view.fragment.ConverterFragmentModule
import com.revolut.android.ui.fragment.converter.ConverterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentProvider {

    @ContributesAndroidInjector(modules = [ConverterFragmentModule::class])
    abstract fun provideConverterFragment(): ConverterFragment

}