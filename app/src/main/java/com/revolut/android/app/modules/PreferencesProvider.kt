package com.revolut.android.app.modules

import android.app.Application
import com.revolut.android.app.Preferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PreferencesProvider {

    @Provides
    @Singleton
    fun providePreferences(app: Application): Preferences {
        return Preferences(app)
    }

}