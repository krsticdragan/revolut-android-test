package com.revolut.android.app.modules.view.fragment

import com.revolut.android.ui.fragment.converter.ConverterContract
import com.revolut.android.ui.fragment.converter.ConverterFragment
import com.revolut.android.ui.fragment.converter.ConverterPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class ConverterFragmentModule {

    @Binds
    abstract fun provideConverterFragmentView(converterFragment: ConverterFragment): ConverterContract.View

    @Binds
    abstract fun provideConverterFragmentPresenter(converterPresenter: ConverterPresenter): ConverterContract.Presenter

}