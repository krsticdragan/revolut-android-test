package com.revolut.android.app.modules

import com.revolut.android.app.modules.view.activity.MainActivityModule
import com.revolut.android.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityProvider {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun provideMainActivity(): MainActivity
}