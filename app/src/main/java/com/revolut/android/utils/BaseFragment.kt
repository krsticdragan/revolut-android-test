package com.revolut.android.utils

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.revolut.android.R
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment : DaggerFragment() {

    private lateinit var noInternetConnectionDialog: AlertDialog
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    abstract fun getLayoutResource(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (getLayoutResource() != -1) {
            return inflater.inflate(getLayoutResource(), container, false)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    fun showToast(text: String?) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

}