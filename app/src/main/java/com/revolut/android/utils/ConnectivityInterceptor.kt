package com.revolut.android.utils

import android.content.Context
import com.revolut.android.app.modules.NetworkProvider
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptor(private val context: Context, private val networkProvider: NetworkProvider) :
    Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!networkProvider.isOnline(context)) {
            throw NoConnectivityException(context)
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}