package com.revolut.android.utils

object Constants {

    //region api
    const val BASE_URL = "https://revolut.duckdns.org/"

    //region preferences
    const val DEFAULT_CURRENCY = "EUR"
    const val PREF_LAST_CURRENCY = "PREF_LAST_CURRENCY"
    const val PREF_LAST_CURRENCY_VALUE = "PREF_LAST_CURRENCY_VALUE"

}