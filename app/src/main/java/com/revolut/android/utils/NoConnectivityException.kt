package com.revolut.android.utils

import android.content.Context
import com.revolut.android.R
import java.io.IOException


class NoConnectivityException(private var context: Context) : IOException() {

    override fun getLocalizedMessage(): String {
        return context.resources.getString(R.string.error_no_internet_connection)
    }
}