package com.revolut.android.utils

import com.revolut.android.app.App
import java.util.*

object CurrencyUtils {

    fun getCurrencyImage(currency: String): Int {
        return App.instance.resources.getIdentifier(
            "ic_${currency.toLowerCase(Locale.getDefault())}",
            "drawable",
            App.instance.packageName
        )
    }

    fun getCurrencyFullName(currencyName: String): String {
        return when (currencyName) {
            "EUR" -> "Euro"
            "AUD" -> "Australian Dollar"
            "BRL" -> "Brazilian Real"
            "BGN" -> "Bulgarian Lev"
            "CAD" -> "Canadian Dollar"
            "CHF" -> "Swiss Franc"
            "CNY" -> "Chinese Yuan"
            "CZK" -> "Czech Republic Koruna"
            "DKK" -> "Danish Krone"
            "GBP" -> "British Pound Sterling"
            "HKD" -> "Hong Kong Dollar"
            "HRK" -> "Croatian Kuna"
            "HUF" -> "Hungarian Forint"
            "IDR" -> "Indonesian Rupiah"
            "ILS" -> "Israeli New Sheqel"
            "INR" -> "Indian Rupee"
            "ISK" -> "Icelandic Króna"
            "JPY" -> "Japanese Yen"
            "KRW" -> "South Korean Won"
            "MXN" -> "Mexican Peso"
            "MYR" -> "Malaysian Ringgit"
            "NOK" -> "Norwegian Krone"
            "NZD" -> "New Zealand Dollar"
            "PHP" -> "Philippine Peso"
            "PLN" -> "Polish Zloty"
            "RON" -> "Romanian Leu"
            "RUB" -> "Russian Ruble"
            "SEK" -> "Swedish Krona"
            "SGD" -> "Singapore Dollar"
            "THB" -> "Thai Baht"
            "TRY" -> "Turkish Lira"
            "USD" -> "United State Dollar"
            "ZAR" -> "South African Rand"
            else -> "N/A"
        }
    }

}