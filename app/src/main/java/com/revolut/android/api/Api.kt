package com.revolut.android.api

import com.revolut.android.model.ConvertRatesResponse
import com.revolut.android.utils.Constants
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("latest")
    fun getConvertData(@Query("base") currency: String): Observable<ConvertRatesResponse>

    companion object Factory {
        fun create(): Api {
            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build()

            return retrofit.create(Api::class.java)
        }
    }
}