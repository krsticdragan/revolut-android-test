package com.revolut.android.model

data class Currency(
    val currencyName: String,
    val currencyRate: Double
)