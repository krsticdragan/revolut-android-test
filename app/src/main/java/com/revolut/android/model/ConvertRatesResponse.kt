package com.revolut.android.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ConvertRatesResponse {

    @SerializedName("base")
    @Expose
    var base: String? = null
    @SerializedName("date")
    @Expose
    var date: String? = null
    @SerializedName("rates")
    @Expose
    var rates: MutableMap<String, Double>? = null

    fun getRateList(): MutableList<Currency> {
        val list = mutableListOf<Currency>()
        rates!!.keys.forEach {
            list.add(Currency(it, rates!![it]!!))
        }
        return list
    }

}